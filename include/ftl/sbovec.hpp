#ifndef FTL_SBO_VECTOR_HPP
#define FTL_SBO_VECTOR_HPP

#include "config.hpp"
#include "memory.hpp"
#include <initializer_list>
#include <limits>
#include <cassert>

#if __STDC_HOSTED__ == 1 || defined(__cpp_lib_freestanding_optional)
#define FTL_HAS_OPTIONAL
#include <optional>
#endif

#ifndef FTL_GROW_FACTOR
#define FTL_GROW_FACTOR 2
#endif

namespace FTL_NAMESPACE
{
    template <typename T>
    constexpr static std::size_t default_sbo_size_for = 2 * sizeof(T*) / sizeof(T);

    static_assert(default_sbo_size_for<int> == 4);

    template <typename T, typename AllocT = FTL_DEFAULT_ALLOCATOR<T>, std::size_t SBO_Size = default_sbo_size_for<T>>
    class sbovec : detail::inherit_allocator<AllocT>
    {
        constexpr static bool allocation_can_throw = FTL_EXCEPTIONS_ENABLED && (
                !FTL_NOTHROW_LENGTH_ERRORS
             || !std::is_nothrow_move_assignable_v<T>
         );

        public:
            using value_type        = T;
            using reference         = T&;
            using const_reference   = const T&;

            using pointer           = T*;
            using const_pointer     = const T*;

            using difference_type   = std::ptrdiff_t;
            using size_type         = std::size_t;

            using iterator          = pointer;
            using const_iterator    = const_pointer;

            using allocator_type    = AllocT;

            constexpr static size_type sbo_limit = SBO_Size;

            constexpr sbovec() noexcept(noexcept(allocator_type())) : sbovec(allocator_type()) {}
            explicit constexpr sbovec(const allocator_type&) noexcept requires std::allocator_traits<allocator_type>::is_always_equal::value;
            explicit constexpr sbovec(const allocator_type&) noexcept requires (not std::allocator_traits<allocator_type>::is_always_equal::value);

            explicit constexpr sbovec(size_type count, const allocator_type& = allocator_type());
            explicit constexpr sbovec(size_type count, const_reference, const allocator_type& = allocator_type());

            constexpr sbovec(const sbovec&)
                noexcept(not allocation_can_throw && std::is_nothrow_copy_constructible_v<value_type>);
            constexpr sbovec(sbovec&&)
                noexcept(std::is_nothrow_move_constructible_v<value_type>);
            constexpr sbovec& operator=(const sbovec&)
                noexcept(not allocation_can_throw && std::is_nothrow_copy_assignable_v<value_type>);
            constexpr sbovec& operator=(sbovec&&)
                noexcept(std::is_nothrow_move_assignable_v<value_type>);

            constexpr sbovec(std::initializer_list<value_type>) noexcept(not allocation_can_throw);

            constexpr ~sbovec() noexcept;

            constexpr bool operator==(const sbovec& rhs) const noexcept;
            constexpr bool operator!=(const sbovec& rhs) const noexcept { return not (*this == rhs); }

            [[nodiscard]] constexpr size_type capacity() const noexcept;
            [[nodiscard]] constexpr size_type size() const noexcept;
            [[nodiscard]] constexpr size_type max_size() const noexcept;

            constexpr size_type reserve(size_type) noexcept(not allocation_can_throw);

            constexpr void resize(size_type)
                noexcept(not allocation_can_throw
                          && std::is_nothrow_default_constructible_v<value_type>);

            constexpr void resize(size_type, const_reference)
                noexcept(not allocation_can_throw
                          && std::is_nothrow_copy_constructible_v<value_type>);

            constexpr void clear() noexcept(std::is_nothrow_destructible_v<value_type>) { drop_elements(); }

            [[nodiscard]] constexpr allocator_type get_allocator() const
                noexcept(std::is_nothrow_copy_constructible_v<allocator_type>) { return this->allocator(); };

            /*
             *  queries
             */
            [[nodiscard]] constexpr bool is_empty() const noexcept;
            [[nodiscard]] constexpr bool empty() const noexcept { return is_empty(); }

            [[nodiscard]] constexpr bool is_full() const noexcept;
            [[nodiscard]] constexpr bool full() const noexcept { return is_full(); }

            [[nodiscard]] constexpr pointer data() noexcept { return get_pointer(); }
            [[nodiscard]] constexpr const_pointer data() const noexcept { return get_pointer(); }

            [[nodiscard]] constexpr bool is_small() const noexcept {
                if (std::is_constant_evaluated()) {
                    return false;
                }
                return !meta.tag;
            }

            /*
             *  iterators
             */
            [[nodiscard]] constexpr iterator begin() noexcept;
            [[nodiscard]] constexpr iterator end() noexcept;
            [[nodiscard]] constexpr const_iterator begin() const noexcept;
            [[nodiscard]] constexpr const_iterator end() const noexcept;
            [[nodiscard]] constexpr const_iterator cbegin() const noexcept { return const_cast<const sbovec&>(*this).begin(); }
            [[nodiscard]] constexpr const_iterator cend() const noexcept { return const_cast<const sbovec&>(*this).end(); }

            /*
             *  element access
             */
            [[nodiscard]] constexpr reference front() noexcept(FTL_NOTHROW_BOUNDS_ERRORS);
            [[nodiscard]] constexpr reference back() noexcept(FTL_NOTHROW_BOUNDS_ERRORS);
            [[nodiscard]] constexpr const_reference front() const noexcept(FTL_NOTHROW_BOUNDS_ERRORS);
            [[nodiscard]] constexpr const_reference back() const noexcept(FTL_NOTHROW_BOUNDS_ERRORS);

            [[nodiscard]] constexpr reference at(size_type);
            [[nodiscard]] constexpr const_reference at(size_type) const;

            [[nodiscard]] constexpr reference operator[](size_type) noexcept(FTL_NOTHROW_BOUNDS_ERRORS);
            [[nodiscard]] constexpr const_reference operator[](size_type) const noexcept(FTL_NOTHROW_BOUNDS_ERRORS);

            /*
             *  push / pop
             */
            constexpr void push_back(const value_type&)
                noexcept(not allocation_can_throw && std::is_nothrow_copy_assignable_v<value_type>);

            constexpr void push_back(value_type&&)
                noexcept(not allocation_can_throw && std::is_nothrow_move_assignable_v<value_type>);

            template <typename... Args>
            constexpr void emplace_back(Args&&... args)
                noexcept(not allocation_can_throw && std::is_nothrow_constructible_v<value_type, Args&&...>);

            constexpr value_type pop_back() noexcept(std::is_nothrow_move_assignable_v<value_type>);

            #ifdef FTL_HAS_OPTIONAL
            constexpr std::optional<value_type> try_pop_back()
                noexcept(std::is_nothrow_move_assignable_v<value_type>
                      && std::is_nothrow_destructible_v<value_type>);
            #endif

            constexpr void shrink_to_fit() noexcept requires (not supports_shrinking<allocator_type>) {}
            constexpr void shrink_to_fit() noexcept requires supports_shrinking<allocator_type>;

            /*
             *  insert
             */
            template <typename... Args>
            constexpr iterator emplace(const_iterator pos, Args&&... args)
                noexcept(not allocation_can_throw && std::is_nothrow_constructible_v<value_type, Args&&...>);

            constexpr iterator insert(const_iterator pos, const_reference value)
                noexcept(not allocation_can_throw && std::is_nothrow_copy_constructible_v<value_type>);
            constexpr iterator insert(const_iterator pos, value_type&& value)
                noexcept(not allocation_can_throw && std::is_nothrow_move_constructible_v<value_type>);
            constexpr iterator insert(const_iterator pos, size_type count, const_reference value);

            template <typename InputIt>
            constexpr iterator insert(const_iterator pos, InputIt first, InputIt last)
                noexcept(not allocation_can_throw && std::is_nothrow_copy_constructible_v<value_type>);

            constexpr iterator insert(const_iterator pos, std::initializer_list<value_type> list)
                noexcept(not allocation_can_throw && std::is_nothrow_move_constructible_v<value_type>);

            /*
             *  erase
             */
            constexpr iterator erase(const_iterator pos)
                noexcept(std::is_nothrow_move_assignable_v<value_type>);
            constexpr iterator erase(const_iterator first, const_iterator last)
                noexcept(std::is_nothrow_move_assignable_v<value_type>);

            constexpr void swap(sbovec& rhs) noexcept(std::is_nothrow_move_constructible_v<value_type>);

            /*
             *  TODO: FOLLOWING STILL UNIMPLEMENTED
             */

            #if __cpp_lib_containers_ranges >= 202202
            template <typename R>
            constexpr iterator insert_range(const_iterator pos, R&& range);

            template <typename R>
            constexpr iterator append_range(R&& range);
            #endif

        private:
            static_assert(std::contiguous_iterator<iterator>);
            static_assert(std::contiguous_iterator<const_iterator>);

            struct sbo_metadata {
                size_type   value   : (sizeof(size_type) * 8) - 1 = 0;
                bool        tag     : 1 = false;

                void set_value(size_type new_value) noexcept {
                    value = new_value & ~(0b1ULL << (sizeof(size_type) * 8 - 1));
                }
            };

            static_assert(sizeof(sbo_metadata) == sizeof(size_type));

            #ifndef __cpp_trivial_union
            template <std::size_t Count, bool IsTrivial = std::is_trivially_constructible_v<T>>
            union union_storage {};

            template <std::size_t Count> requires (Count > 0)
            union union_storage<Count, true /*trivial T*/> {
                T data[Count];
            };

            template <std::size_t Count> requires (Count > 0)
            union union_storage<Count, false /*nontrivial T*/> {
                constexpr union_storage() {}
                constexpr ~union_storage() {}
                constexpr union_storage(const union_storage&) = default;
                constexpr union_storage(union_storage&&) = default;
                constexpr union_storage& operator=(const union_storage&) = default;
                constexpr union_storage& operator=(union_storage&&) = default;

                T data[Count];
            };

            #else
            template <std::size_t Count>
            union union_storage {
                T data[Count];
            };
            #endif

            struct sbo_external_storage
            {
                T* first{};
                T* last{};
            };


            constexpr void make_small() noexcept {
                meta.tag = false;
            }

            constexpr void make_large() noexcept {
                meta.tag = true;
            }

            constexpr void grow() noexcept(not allocation_can_throw)
            {
                if (capacity() == max_size()) {
                    FTL_LENGTH_ERROR("sbovec attempt to grow at max_size()");
                }
                else if (capacity() == 0) {
                    reserve(1);
                } else {
                    std::size_t attempted_size = capacity() * FTL_GROW_FACTOR <= capacity()
                        ? max_size()
                        : capacity() * FTL_GROW_FACTOR;

                    reserve(attempted_size > max_size() ? max_size() : attempted_size);
                }
            }

            constexpr void set_size(size_type new_size) noexcept {
                if (is_small()) {
                    meta.set_value(new_size);
                } else {
                    external.last = external.first + new_size;
                }
            }

            constexpr void drop_elements() noexcept(std::is_trivially_destructible_v<value_type>
                                                 || std::is_nothrow_destructible_v<value_type>)
            {
                if constexpr (not std::is_trivially_destructible_v<value_type>) {
                    for (auto& elem : *this) {
                        std::allocator_traits<allocator_type>::destroy(this->alloc(), &elem);
                    }
                }
                set_size(0);
            }

            constexpr void drop_current_storage() {
                drop_elements();

                if (not is_small()) {
                    if (data() == nullptr)
                        return;

                    std::allocator_traits<allocator_type>::deallocate(this->alloc(), data(), capacity());
                    external.first = nullptr;
                    external.last = nullptr;
                    make_small();
                    meta.set_value(0);
                }
            }

            [[nodiscard]] constexpr bool fits_in_sbo(size_type req_size) noexcept {
                if (std::is_constant_evaluated()) {
                    return false;
                }
                return req_size <= sbo_limit;
            }

            constexpr pointer get_pointer() noexcept {
                if (std::is_constant_evaluated()) {
                    return external.first;
                }
                if constexpr(sbo_limit == 0) {
                    return external.first;
                } else {
                    if (is_small()) {
                        return &internal.data[0];
                    } else {
                        return external.first;
                    }
                }
            }

            [[nodiscard]] constexpr const_pointer get_pointer() const noexcept {
                if (std::is_constant_evaluated()) {
                    return external.first;
                }
                if constexpr(sbo_limit == 0) {
                    return external.first;
                } else {
                    if (is_small()) {
                        return &internal.data[0];
                    } else {
                        return external.first;
                    }
                }
            }

            template <typename U>
            constexpr void copy_from_container(U& container) {
                auto* ptr = data();
                for (auto&& val : container)
                    std::allocator_traits<allocator_type>::construct(this->alloc(), ptr++, FTL_MOVE(val));

                set_size(container.size());
            }

            template <typename U>
            constexpr void copy_from_container(const U& container) {
                auto* ptr = data();
                for (auto&& val : container)
                    std::allocator_traits<allocator_type>::construct(this->alloc(), ptr++, val);

                set_size(container.size());
            }

            sbo_metadata meta;

            union {
                sbo_external_storage    external{};
                union_storage<SBO_Size> internal;
            };
    };

    /*
     *  Implementations follow
     */

    /*
     *  Constructors
     */
    template <typename T, typename A, std::size_t N>
    constexpr sbovec<T,A,N>::sbovec(const allocator_type&) noexcept
        requires std::allocator_traits<allocator_type>::is_always_equal::value
    {
        if (std::is_constant_evaluated()) {
            make_large();
            return;
        }
    }

    template <typename T, typename A, std::size_t N>
    constexpr sbovec<T,A,N>::sbovec(const allocator_type& alloc) noexcept
        requires (not std::allocator_traits<allocator_type>::is_always_equal::value)
        : detail::inherit_allocator<allocator_type>(alloc)
    {
        if (std::is_constant_evaluated()) {
            make_large();
            return;
        }
    }

    template <typename T, typename A, std::size_t N>
    constexpr sbovec<T,A,N>::sbovec(std::initializer_list<value_type> init_list) noexcept(not allocation_can_throw)
    {
        if (init_list.size() > capacity()) {
            reserve(init_list.size() + init_list.size() / 2);
        }

        copy_from_container(FTL_MOVE(init_list));
    }

    template <typename T, typename A, std::size_t N>
    constexpr sbovec<T,A,N>::sbovec(const sbovec& other)
        noexcept(not allocation_can_throw && std::is_nothrow_copy_constructible_v<value_type>)
    {
        this->alloc() = std::allocator_traits<allocator_type>::select_on_container_copy_construction(other.alloc());
        reserve(other.size());
        copy_from_container(other);
    }

    template <typename T, typename A, std::size_t N>
    constexpr sbovec<T,A,N>::sbovec(sbovec&& other)
        noexcept(std::is_nothrow_move_constructible_v<value_type>)
    {
        this->alloc() = FTL_MOVE(other.alloc());
        if (other.is_small()) {
            copy_from_container(other);
        } else {
            make_large();

            std::swap(meta, other.meta);
            std::swap(external, other.external);
        }
    }

    template <typename T, typename A, std::size_t N>
    constexpr sbovec<T,A,N>::sbovec(size_type count, const allocator_type& allocator)
        : sbovec(allocator)
    {
        resize(count);
    }

    template <typename T, typename A, std::size_t N>
    constexpr sbovec<T,A,N>::sbovec(size_type count, const T& elem, const allocator_type& allocator)
        : sbovec(allocator)
    {
        resize(count, elem);
    }

    template <typename T, typename A, std::size_t N>
    constexpr sbovec<T,A,N>::~sbovec() noexcept {
        drop_current_storage();
    }

    /*
     *  Assignment
     */
    template <typename T, typename A, std::size_t N>
    constexpr sbovec<T,A,N>& sbovec<T,A,N>::operator=(const sbovec& other)
        noexcept(not allocation_can_throw && std::is_nothrow_copy_assignable_v<value_type>)
    {
        if (&other == this)
            return *this;

        clear();

        if constexpr (std::allocator_traits<allocator_type>::propagate_on_container_copy_assignment::value)
            this->alloc() = std::allocator_traits<allocator_type>::select_on_container_copy_construction(other.alloc());

        reserve(other.size());
        copy_from_container(other);
        return *this;
    }

    template <typename T, typename A, std::size_t N>
    constexpr sbovec<T,A,N>& sbovec<T,A,N>::operator=(sbovec&& other)
        noexcept(std::is_nothrow_move_assignable_v<value_type>)
    {
        if (&other == this)
            return *this;

        drop_current_storage();

        if constexpr (std::allocator_traits<allocator_type>::propagate_on_container_copy_assignment::value)
            this->alloc() = FTL_MOVE(other.alloc());

        if (other.is_small()) {
            copy_from_container(other);
            return *this;
        }

        std::swap(meta, other.meta);
        std::swap(external, other.external);

        return *this;
    }

    /*
     *  Comparison
     */
    template <typename T, typename A, std::size_t N>
    constexpr bool sbovec<T,A,N>::operator==(const sbovec& rhs) const noexcept {
        if (size() != rhs.size())
            return false;

        const_iterator left_it = begin();
        const_iterator right_it = rhs.begin();
        while (left_it != end()) {
            if (*left_it++ != *right_it++)
                return false;
        }
        return true;
    }

    /*
     *  Iterators
     */
    template <typename T, typename A, std::size_t N>
    constexpr sbovec<T,A,N>::iterator sbovec<T,A,N>::begin() noexcept {
        return get_pointer();
    }

    template <typename T, typename A, std::size_t N>
    constexpr sbovec<T,A,N>::const_iterator sbovec<T,A,N>::begin() const noexcept {
        return get_pointer();
    }

    template <typename T, typename A, std::size_t N>
    constexpr sbovec<T,A,N>::iterator sbovec<T,A,N>::end() noexcept {
        return get_pointer() + size();
    }

    template <typename T, typename A, std::size_t N>
    constexpr sbovec<T,A,N>::const_iterator sbovec<T,A,N>::end() const noexcept {
        return get_pointer() + size();
    }

    template <typename T, typename A, std::size_t N>
    constexpr sbovec<T,A,N>::size_type sbovec<T,A,N>::capacity() const noexcept {
        if (is_small()) {
            return sbo_limit;
        } else {
            return meta.value;
        }
    }

    template <typename T, typename A, std::size_t N>
    constexpr sbovec<T,A,N>::size_type sbovec<T,A,N>::reserve(size_type count) noexcept(not allocation_can_throw)
    {
        if (count <= capacity())
            return capacity();

        size_type old_size = size();
        #if __cpp_lib_allocate_at_least >= 202302L
        auto result = std::allocator_traits<allocator_type>::allocate(this->alloc(), count);
        auto new_ptr = result.ptr;
        size_type allocation_size = result.count;
        #else
        auto new_ptr = std::allocator_traits<allocator_type>::allocate(this->alloc(), count);
        size_type allocation_size = count;
        #endif

        for (std::size_t i = 0; i < old_size; ++i)
            std::allocator_traits<allocator_type>::construct(this->alloc(), new_ptr + i, FTL_MOVE(*(data() + i)));

        drop_current_storage();

        make_large();
        external.first = new_ptr;
        external.last = new_ptr + old_size;
        meta.set_value(allocation_size);

        return allocation_size;
    }

    template <typename T, typename A, std::size_t N>
    constexpr void sbovec<T,A,N>::resize(size_type new_size)
        noexcept(not allocation_can_throw
                  && std::is_nothrow_default_constructible_v<value_type>)
    {
        if (new_size == size())
            return;
        else if (new_size < size()) {
            while (new_size < size()) {
                pop_back();
            }
        }
        else {
            size_type old_size = size();
            reserve(new_size);
            for (size_type i = old_size; i < new_size; ++i)
                std::allocator_traits<allocator_type>::construct(this->alloc(), data() + i);

            set_size(new_size);
        }
    }

    template <typename T, typename A, std::size_t N>
    constexpr void sbovec<T,A,N>::resize(size_type new_size, const_reference value)
        noexcept(not allocation_can_throw
                  && std::is_nothrow_copy_constructible_v<value_type>)
    {
        if (new_size == size())
            return;
        else if (new_size < size()) {
            while (new_size < size()) {
                pop_back();
            }
        }
        else {
            size_type old_size = size();
            reserve(new_size);
            for (size_type i = old_size; i < new_size; ++i)
                std::allocator_traits<allocator_type>::construct(this->alloc(), data() + i, value);

            set_size(new_size);
        }
    }

    template <typename T, typename A, std::size_t N>
    constexpr sbovec<T,A,N>::size_type sbovec<T,A,N>::size() const noexcept {
        if (is_small()) {
            return meta.value;
        } else {
            return static_cast<size_type>(external.last - external.first);
        }
    }

    template <typename T, typename A, std::size_t N>
    constexpr sbovec<T,A,N>::size_type sbovec<T,A,N>::max_size() const noexcept {
        return (std::numeric_limits<size_type>::max() & ~(0b1ULL << (sizeof(size_type) * 8 - 1))) / sizeof(T);
    }

    template <typename T, typename A, std::size_t N>
    constexpr bool sbovec<T,A,N>::is_empty() const noexcept {
        return begin() == end();
    }

    template <typename T, typename A, std::size_t N>
    constexpr bool sbovec<T,A,N>::is_full() const noexcept {
        return size() == capacity();
    }

    static_assert(sizeof(sbovec<int>) <= 2 * sizeof(void*) + sizeof(std::size_t));
    static_assert(sbovec<int>{}.empty());
    static_assert(sbovec<int>{}.size() == 0);

    template <typename T, typename A, std::size_t N>
    constexpr void sbovec<T,A,N>::push_back(const value_type& value)
        noexcept(not allocation_can_throw && std::is_nothrow_copy_assignable_v<value_type>)
    {
        if (is_full())
            grow();

        std::allocator_traits<allocator_type>::construct(this->alloc(), data() + size(), value);
        set_size(size() + 1);
    }

    template <typename T, typename A, std::size_t N>
    constexpr void sbovec<T,A,N>::push_back(value_type&& value)
        noexcept(not allocation_can_throw && std::is_nothrow_move_assignable_v<value_type>)
    {
        if (is_full())
            grow();

        std::allocator_traits<allocator_type>::construct(this->alloc(), data() + size(), FTL_MOVE(value));
        set_size(size() + 1);
    }

    template <typename T, typename A, std::size_t N>
    template <typename... Args>
    constexpr void sbovec<T,A,N>::emplace_back(Args&&... args)
        noexcept(not allocation_can_throw && std::is_nothrow_constructible_v<value_type, Args&&...>)
    {
        if (is_full())
            grow();

        std::allocator_traits<allocator_type>::construct(this->alloc(), data() + size(), FTL_FORWARD(args)...);
        set_size(size() + 1);
    }

    template <typename T, typename A, std::size_t N>
    constexpr sbovec<T,A,N>::value_type sbovec<T,A,N>::pop_back()
        noexcept(std::is_nothrow_move_assignable_v<value_type>)
    {
        if (size() == 0)
            FTL_BOUNDS_ERROR();

        value_type temp = FTL_MOVE(back());
        std::allocator_traits<allocator_type>::destroy(this->alloc(), &back());
        set_size(size() - 1);
        return temp;
    }

    #ifdef FTL_HAS_OPTIONAL
    template <typename T, typename A, std::size_t N>
    constexpr std::optional<typename sbovec<T,A,N>::value_type> sbovec<T,A,N>::try_pop_back()
        noexcept(std::is_nothrow_move_assignable_v<value_type>
              && std::is_nothrow_destructible_v<value_type>)
    {
        if (size() == 0)
            return std::nullopt;

        value_type temp = FTL_MOVE(back());
        std::allocator_traits<allocator_type>::destroy(this->alloc(), &back());
        set_size(size() - 1);
        return temp;
    }
    #endif

    /*
     *  access
     */
    template <typename T, typename A, std::size_t N>
    constexpr sbovec<T,A,N>::reference sbovec<T,A,N>::front() noexcept(FTL_NOTHROW_BOUNDS_ERRORS)
    {
        if (size() == 0)
            FTL_BOUNDS_ERROR("called front() to an empty sbovec");

        return *data();
    }

    template <typename T, typename A, std::size_t N>
    constexpr sbovec<T,A,N>::reference sbovec<T,A,N>::back() noexcept(FTL_NOTHROW_BOUNDS_ERRORS)
    {
        if (size() == 0)
            FTL_BOUNDS_ERROR("called back() to an empty sbovec");

        return *(data() + size() - 1);
    }

    template <typename T, typename A, std::size_t N>
    constexpr sbovec<T,A,N>::const_reference sbovec<T,A,N>::front() const noexcept(FTL_NOTHROW_BOUNDS_ERRORS)
    {
        if (size() == 0)
            FTL_BOUNDS_ERROR("called front() to an empty sbovec");

        return *data();
    }

    template <typename T, typename A, std::size_t N>
    constexpr sbovec<T,A,N>::const_reference sbovec<T,A,N>::back() const noexcept(FTL_NOTHROW_BOUNDS_ERRORS)
    {
        if (size() == 0)
            FTL_BOUNDS_ERROR("called back() to an empty sbovec");

        return *(data() + size() - 1);
    }

    template <typename T, typename A, std::size_t N>
    constexpr sbovec<T,A,N>::reference sbovec<T,A,N>::at(size_type idx) {
        if (idx >= size())
            FTL_THROW_OR_PANIC(std::out_of_range("sbovec access out of bounds"));
        return *(data() + idx);
    }

    template <typename T, typename A, std::size_t N>
    constexpr sbovec<T,A,N>::const_reference sbovec<T,A,N>::at(size_type idx) const {
        if (idx >= size())
            FTL_THROW_OR_PANIC(std::out_of_range("sbovec access out of bounds"));
        return *(data() + idx);
    }

    template <typename T, typename A, std::size_t N>
    constexpr sbovec<T,A,N>::reference sbovec<T,A,N>::operator[](size_type idx) noexcept(FTL_NOTHROW_BOUNDS_ERRORS) {
        if (idx >= size())
            FTL_BOUNDS_ERROR("sbovec[] index out of range")
        return *(data() + idx);
    }

    template <typename T, typename A, std::size_t N>
    constexpr sbovec<T,A,N>::const_reference sbovec<T,A,N>::operator[](size_type idx) const noexcept(FTL_NOTHROW_BOUNDS_ERRORS) {
        if (idx >= size())
            FTL_BOUNDS_ERROR("sbovec[] index out of range")
        return *(data() + idx);
    }

    #ifdef FTL_HAS_OPTIONAL
    /*
    TODO: needs C++ standard library support that is not standardised or implemented yet,
    remove the comments once we have a feature check macro for std::optional<T&>

    constexpr std::optional<reference> try_front() noexcept {
        if (size() == 0)
            return std::nullopt;

        return *data();
    }

    constexpr std::optional<reference> try_back() noexcept {
        if (size() == 0)
            return std::nullopt;

        return *(data() + size());
    }

    constexpr std::optional<const_reference> try_front() const noexcept {
        if (size() == 0)
            return std::nullopt;

        return *data();
    }

    constexpr std::optional<const_reference> try_back() const noexcept {
        if (size() == 0)
            return std::nullopt;

        return *(data() + size());
    }
    */
    #endif

    /*
     *  insert etc.
     */
    template <typename T, typename A, std::size_t N>
    template <typename... Args>
    constexpr sbovec<T,A,N>::iterator sbovec<T,A,N>::emplace(const_iterator pos, Args&&... args)
        noexcept(not allocation_can_throw && std::is_nothrow_constructible_v<value_type, Args&&...>)
    {
        if (pos == end()) {
            std::size_t old_size = size();
            emplace_back(FTL_FORWARD(args)...);
            return iterator(begin() + old_size);
        } else {
            auto offset = static_cast<std::size_t>(pos - begin());
            // std::move_backwards
            if (is_full())
                grow();

            iterator last = end();
            iterator first = begin() + offset;
            iterator d_last = end() + 1;
            while (first != last)
                std::allocator_traits<allocator_type>::construct(this->alloc(), --d_last, FTL_MOVE(*(--last))); // NOLINT: intended

            std::allocator_traits<allocator_type>::construct(this->alloc(), --d_last, FTL_FORWARD(args)...);
            set_size(size() + 1);
            return d_last;
        }
    }

    template <typename T, typename A, std::size_t N>
    constexpr sbovec<T,A,N>::iterator sbovec<T,A,N>::insert(const_iterator pos, const_reference value)
        noexcept(not allocation_can_throw && std::is_nothrow_copy_constructible_v<value_type>)
    {
        return emplace(pos, value);
    }

    template <typename T, typename A, std::size_t N>
    constexpr sbovec<T,A,N>::iterator sbovec<T,A,N>::insert(const_iterator pos, value_type&& value)
        noexcept(not allocation_can_throw && std::is_nothrow_move_constructible_v<value_type>)
    {
        return emplace(pos, FTL_MOVE(value));
    }

    template <typename T, typename A, std::size_t N>
    template <typename InputIt>
    constexpr sbovec<T,A,N>::iterator sbovec<T,A,N>::insert(const_iterator pos,
                                                            InputIt first,
                                                            InputIt last)
        noexcept(not allocation_can_throw && std::is_nothrow_copy_constructible_v<value_type>)
    {
        if (pos == end()) {
            std::size_t offset = size();
            for (auto it = first; it != last; ++it) {
                emplace_back(*it);
            }
            return begin() + offset;
        } else {
            auto offset = static_cast<std::size_t>(pos - begin());
            auto signed_count = std::distance(first, last);
            if (signed_count < 0)
                FTL_LOGIC_ERROR("invalid iterators given to sbovec::insert");

            auto count = static_cast<std::size_t>(signed_count);

            if (capacity() < size() + count) {
                reserve(capacity() + count);
            }

            iterator reloc_last = end();
            iterator reloc_first = begin() + offset;
            iterator head = end() + count;
            while (reloc_first != reloc_last)
                std::allocator_traits<allocator_type>::construct(this->alloc(), --head, FTL_MOVE(*(--reloc_last))); // NOLINT: intended

            iterator rv = begin() + offset;
            head = rv;
            while (head != rv + count)
                std::allocator_traits<allocator_type>::construct(this->alloc(), head++, *first++);

            set_size(size() + count);
            return rv;
        }
    }

    template <typename T, typename A, std::size_t N>
    constexpr sbovec<T,A,N>::iterator sbovec<T,A,N>::insert(const_iterator pos,
                                                            std::initializer_list<value_type> list)
        noexcept(not allocation_can_throw && std::is_nothrow_move_constructible_v<value_type>)
    {
        return insert(pos, std::make_move_iterator(list.begin()), std::make_move_iterator(list.end()));
    }

    template <typename T, typename A, std::size_t N>
    constexpr sbovec<T,A,N>::iterator sbovec<T,A,N>::erase(const_iterator first, const_iterator last)
        noexcept(std::is_nothrow_move_assignable_v<value_type>)
    {
        auto offset = static_cast<std::size_t>(first - begin());
        auto signed_count = std::distance(first, last);
        if (signed_count < 0)
            FTL_LOGIC_ERROR("invalid iterators given to sbovec::insert");
        auto count = static_cast<std::size_t>(signed_count);

        iterator head = begin() + offset;
        iterator reloc_first = head + count;
        iterator reloc_last = end();
        while(reloc_first != reloc_last)
            *head++ = FTL_MOVE(*reloc_first++); // NOLINT

        set_size(size() - count);
        return begin() + offset;
    }

    template <typename T, typename A, std::size_t N>
    constexpr sbovec<T,A,N>::iterator sbovec<T,A,N>::erase(const_iterator pos)
        noexcept(std::is_nothrow_move_assignable_v<value_type>)
    {
        if (pos == end())
            FTL_LOGIC_ERROR("single iterator erase() argument not dereferenceable");

        return erase(pos, pos + 1);
    }

    /*
     *  swap
     */
    template <typename T, typename A, std::size_t N>
    constexpr void sbovec<T,A,N>::swap(sbovec& rhs)
        noexcept(std::is_nothrow_move_constructible_v<value_type>)
    {
        if constexpr (std::allocator_traits<allocator_type>::propagate_on_container_swap::value
                   || std::allocator_traits<allocator_type>::is_always_equal::value)
        {
            auto tmp = FTL_MOVE(rhs);
            rhs = FTL_MOVE(*this);
            *this = FTL_MOVE(tmp);
        } else {
            auto tmp_lhs = sbovec(FTL_MOVE(*this), rhs->get_allocator());
            auto tmp_rhs = sbovec(FTL_MOVE(rhs), this->get_allocator());
            swap(*this, tmp_lhs);
            swap(rhs, tmp_rhs);
        }
    }
}

#endif
/*
    Copyright 2024 Jari Ronkainen

    Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
    associated documentation files (the "Software"), to deal in the Software without restriction, including
    without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
    of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
    conditions:

    The above copyright notice and this permission notice shall be included in all copies or substantial portions
    of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
    INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
    PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
    LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
    OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
    DEALINGS IN THE SOFTWARE.
*/
