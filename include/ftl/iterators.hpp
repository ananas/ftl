#ifndef FTL_ITERATOR_HPP
#define FTL_ITERATOR_HPP

#include "config.hpp"

#include <iterator>

namespace FTL_NAMESPACE
{
    template <typename Underlying>
    class deref_iterator : public Underlying
    {
        public:
            using difference_type   = typename std::iterator_traits<Underlying>::difference_type;
            using value_type        = typename std::remove_pointer_t<typename std::iterator_traits<Underlying>::value_type>;
            using pointer           = typename std::remove_pointer_t<typename std::iterator_traits<Underlying>::pointer>;
            using reference         = std::add_lvalue_reference_t<typename std::remove_pointer_t<typename std::remove_reference_t<typename std::iterator_traits<Underlying>::reference>>>;
            using iterator_category = typename std::iterator_traits<Underlying>::iterator_category;

            using wrapped_iterator  = Underlying;

            constexpr reference operator*() { return *Underlying::operator*(); }
            constexpr auto operator->() { return Underlying::operator*(); }
    };

    template <typename T>
    static deref_iterator<T> make_deref_iterator(T iter) {
        return deref_iterator<T>{iter};
    }

    template <typename Rng> requires std::ranges::range<Rng>
    class deref_range
    {
        public:
            using iterator = deref_iterator<typename Rng::iterator>;
            using const_iterator = deref_iterator<typename Rng::iterator>;

            constexpr deref_range(Rng& ref)
                : rng_begin(make_deref_iterator(ref.begin())),
                  rng_end(make_deref_iterator(ref.end()))
            {}

            constexpr iterator begin()  requires (not std::is_const_v<Rng>) { return rng_begin; }
            constexpr iterator end()    requires (not std::is_const_v<Rng>) { return rng_end; }

            constexpr const_iterator begin() const { rng_begin; }
            constexpr const_iterator end() const { return rng_end; }

        private:
            using iterator_type = std::conditional_t<std::is_const_v<Rng>, const_iterator, iterator>;
            iterator_type rng_begin;
            iterator_type rng_end;
    };
}

#endif
