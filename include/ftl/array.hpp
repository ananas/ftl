#ifndef FTL_ARRAY_HPP
#define FTL_ARRAY_HPP

#include "config.hpp"
#include "utility.hpp"

#include <cstdint>
#include <type_traits>

#include "hash.hpp"

namespace FTL_NAMESPACE
{
    template <typename T, std::size_t... Dimensions>
    class array
    {
        static_assert(sizeof...(Dimensions) > 0);

        template <std::size_t Depth, bool IsConst>
        struct array_access_proxy;

        public:
            using value_type        = T;
            using reference         = T&;
            using const_reference   = const T&;
            using size_type         = std::size_t;
            using difference_type   = std::ptrdiff_t;
            using pointer           = T*;
            using const_pointer     = const T*;
            using iterator          = T*;
            using const_iterator    = const T*;

            constexpr static std::size_t dimension = sizeof...(Dimensions);

            // Size
            constexpr static std::integral_constant<std::size_t, (Dimensions * ...)> size = {};
            constexpr static std::integral_constant<std::size_t, (Dimensions * ...)> max_size = {};
            constexpr static std::integral_constant<std::size_t, (Dimensions * ...)> capacity = {};

            constexpr array() noexcept = default;

            template <typename... Values>
            explicit constexpr array(Values&&... v) noexcept : data_array{ FTL_FORWARD(v)... } {}

            template <std::size_t N, std::size_t... Is>
            explicit constexpr array(T(&arr)[N], ftl::index_sequence<Is...>) noexcept : data_array{ arr[Is]... }
            {}

            template <std::size_t N>
            explicit constexpr array(T(&arr)[N]) noexcept : array(arr, make_index_sequence<sizeof...(Dimensions)>())
            {}

            // element access
            template <std::size_t D = dimension, typename std::enable_if<(D > 1), void>::type* = nullptr>
            [[nodiscard]] constexpr auto operator[](size_type index) noexcept {
                return array_access_proxy<1, false>(*this, index);
            }

            template <std::size_t D = dimension, typename std::enable_if<(D > 1), void>::type* = nullptr>
            [[nodiscard]] constexpr const auto operator[](size_type index) const noexcept {
                return array_access_proxy<1, false>(*this, index);
            }

            template <std::size_t D = dimension, typename std::enable_if<(D == 1), void>::type* = nullptr>
            [[nodiscard]] constexpr const_reference operator[](size_type index) const noexcept {
                if (index >= size())
                    FTL_BOUNDS_ERROR();

                return data_array[index];
            }

            template <std::size_t D = dimension, typename std::enable_if<(D == 1), void>::type* = nullptr>
            [[nodiscard]] constexpr reference operator[](size_type index) noexcept {
                if (index >= size())
                    FTL_BOUNDS_ERROR();

                return data_array[index];
            }

            #if defined(__cpp_multidimensional_subscript) && !defined(DISABLE_CPP23_FEATURES)
            template <typename... Dims>
            [[nodiscard]] constexpr reference operator[](Dims... d) noexcept requires (sizeof...(Dimensions) == sizeof...(Dims)) {
                static_assert(each_convertible_to<size_type, Dims...>());
                return data_array[calc_array_index(static_cast<size_type>(d)...)];
            }

            template <typename... Dims>
            [[nodiscard]] constexpr const_reference operator[](Dims... d) const noexcept requires (sizeof...(Dimensions) == sizeof...(Dims)) {
                static_assert(each_convertible_to<size_type, Dims...>());
                return data_array[calc_array_index(static_cast<size_type>(d)...)];
            }
            #endif

            template <typename... Dims>
            [[nodiscard]] constexpr reference at(Dims&&... d) noexcept {
                static_assert(sizeof...(Dims) == dimension, "trying to access array of different dimension");
                static_assert(each_convertible_to<size_type, Dims...>(), "invalid array index type");

                return data_array[calc_array_index(static_cast<size_type>(d)...)];
            }

            template <typename... Dims>
            [[nodiscard]] constexpr const_reference at(Dims&&... d) const noexcept {
                static_assert(sizeof...(Dims) == dimension, "trying to access array of different dimension");
                static_assert(each_convertible_to<size_type, Dims...>(), "invalid array index type");

                return data_array[calc_array_index(d...)];
            }


            [[nodiscard]] constexpr reference front() noexcept                { return data_array[0]; }
            [[nodiscard]] constexpr const_reference front() const noexcept    { return data_array[0]; }
            [[nodiscard]] constexpr reference back() noexcept                 { return data_array[size() - 1]; }
            [[nodiscard]] constexpr const_reference back() const noexcept     { return data_array[size() - 1]; }
            [[nodiscard]] constexpr pointer data() noexcept                   { return data_array; }
            [[nodiscard]] constexpr const_pointer data() const noexcept       { return data_array; }

            // iterators
            [[nodiscard]] constexpr iterator begin() noexcept { return iterator(data_array); }
            [[nodiscard]] constexpr iterator end() noexcept { return iterator(data_array + size()); };
            [[nodiscard]] constexpr const_iterator begin() const noexcept { return iterator(data_array); }
            [[nodiscard]] constexpr const_iterator end() const noexcept { return iterator(data_array + size()); };

            // queries
            [[nodiscard]] constexpr static bool is_empty() noexcept { return size() == 0; }
            [[nodiscard]] constexpr static bool empty() noexcept { return size() == 0; }

            [[nodiscard]] constexpr static size_type byte_size() noexcept { return size() * sizeof(T); }
            [[nodiscard]] constexpr static auto extents() noexcept { return ftl::array(dim_size); }
            [[nodiscard]] constexpr static size_type extents(size_type idx) noexcept { return idx < sizeof...(Dimensions) ? dim_size[idx] : 0; }

            // operations
            constexpr void fill(const T& value) noexcept(std::is_nothrow_copy_constructible_v<T>);
            constexpr void swap(array& other) noexcept(std::is_nothrow_swappable_v<T>);

            [[nodiscard]] constexpr uint64_t hash() const noexcept { return ::ftl::hash(*this); }

            friend constexpr bool operator==(const array<T, Dimensions...>& lhs, const array<T, Dimensions...>& rhs) noexcept {
                for (std::size_t i = 0; i < (Dimensions * ...); ++i)
                    if (lhs.data_array[i] != rhs.data_array[i])
                        return false;

                return true;
            }

        private:
            template <typename... Dims>
            constexpr static std::size_t stride(std::size_t dim, Dims... d) noexcept {
                if constexpr(sizeof...(Dims))
                    return dim_size[sizeof...(d)] * stride(d...);

                return dim_size[0];
            }

            template <typename... Dims>
            constexpr static size_type calc_array_index(size_type idx, Dims... d) noexcept
            {
                if constexpr(sizeof...(Dims))
                    return idx * stride(static_cast<size_type>(d)...) + calc_array_index(static_cast<size_type>(d)...);

                if (idx >= size())
                    FTL_BOUNDS_ERROR();

                return idx;
            }

            constexpr const_reference fetch_cref(std::size_t idx) const noexcept {
                return data_array[idx];
            }

            constexpr reference fetch_ref(std::size_t idx) noexcept {
                return data_array[idx];
            }

            constexpr static size_type dim_size[] = { Dimensions... };
            value_type data_array[size()];
    };

    // deduction guides
    template <typename T, std::size_t N, std::size_t... Is>
    array(T(&arr)[N], ftl::index_sequence<Is...>) -> array<T, N>;

    template <typename T, std::size_t N>
    array(T(&arr)[N]) -> array<T, N>;

    template <typename T, std::size_t... Dimensions>
    void swap(array<T, Dimensions...>& a, array<T, Dimensions...>& b) noexcept(std::is_nothrow_swappable_v<T>) {
        auto temp = FTL_MOVE(a);
        a = FTL_MOVE(b);
        b = FTL_MOVE(temp);
    }

    template <typename T, std::size_t... Dimensions>
    template <std::size_t Depth, bool IsConst>
    struct array<T, Dimensions...>::array_access_proxy
    {
        using ref_type = std::conditional_t<IsConst, const array&, array&>;

        ref_type        ref;
        size_type       cindex;

        constexpr explicit array_access_proxy(ref_type arr, std::size_t cindex) noexcept : ref(arr), cindex(cindex) {}

        template <std::size_t D = Depth + 1, typename std::enable_if<(!IsConst) && D == array::dimension, void>::type* = nullptr>
        constexpr reference operator[](std::size_t index) && noexcept 
        {
            return ref.data_array[cindex * ref.dim_size[Depth - 1] + index];
        }

        template <std::size_t D = Depth + 1, typename std::enable_if<(IsConst) && D == array::dimension, void>::type* = nullptr>
        constexpr const_reference operator[](std::size_t index) const && noexcept
        {
            return ref.data_array[cindex * ref.dim_size[Depth - 1] + index];
        }

        template <std::size_t D = Depth + 1, typename std::enable_if<D < array::dimension, void>::type* = nullptr>
        constexpr auto operator[](std::size_t index) const && noexcept {
            return array_access_proxy<Depth+1, IsConst>(ref, index + (cindex * ref.dim_size[Depth-1]));
        }
    };

    template <typename T, std::size_t... Dimensions>
    constexpr void array<T, Dimensions...>::fill(const T& value) noexcept(std::is_nothrow_copy_constructible_v<T>)
    {
        for (T& e : data_array)
            e = value;
    }

    template <typename T, std::size_t... Dimensions>
    constexpr void array<T, Dimensions...>::swap(array& other) noexcept(std::is_nothrow_swappable_v<T>)
    {
        ::ftl::swap(*this, other);
    }
}

#endif
/*
    Copyright 2024 Jari Ronkainen

    Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
    associated documentation files (the "Software"), to deal in the Software without restriction, including
    without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
    of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
    conditions:

    The above copyright notice and this permission notice shall be included in all copies or substantial portions
    of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
    INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
    PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
    LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
    OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
    DEALINGS IN THE SOFTWARE.
*/
