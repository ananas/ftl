#ifndef FTL_LIB_CONFIG_HPP
#define FTL_LIB_CONFIG_HPP

#ifndef FTL_NAMESPACE
#define FTL_NAMESPACE ftl
#endif

#if __cpp_exceptions && __EXCEPTIONS && !defined(FTL_DISABLE_EXCEPTIONS)
# ifndef FTL_EXCEPTIONS_ENABLED
#  define FTL_EXCEPTIONS_ENABLED 1
# endif
#else
# define FTL_EXCEPTIONS_ENABLED 0
#endif

#if __cplusplus < 202002L
#define FTL_CXX20(statement)
#else
#define FTL_CXX20(statement) statement
#endif

#if __msvc__
#define FTL_OPT_EMPTY_BASES __declspec(empty_bases)
#else
#define FTL_OPT_EMPTY_BASES
#endif


#if FTL_EXCEPTIONS_ENABLED
# define FTL_THROW_OR_PANIC(x) throw(x)
#else
# if __msvc__
# define FTL_THROW_OR_PANIC(x) std::abort()
#else
# define FTL_THROW_OR_PANIC(x) __builtin_trap()
#endif
#endif

#if !defined(FTL_ERROR_MODE_TRAP) || !defined(FTL_ERROR_MODE_THROW) || !defined(FTL_ERROR_MODE_IGNORE)
# define FTL_ERROR_MODE_TRAP
#endif

#if defined(FTL_ERROR_MODE_TRAP)
# if !defined(FTL_BOUNDS_ERROR_MODE_TRAP) || !defined(FTL_BOUNDS_ERROR_MODE_THROW) || !defined(FTL_BOUNDS_ERROR_MODE_IGNORE)
#  define FTL_BOUNDS_ERROR_MODE_TRAP
# endif
# if !defined(FTL_LOGIC_ERROR_MODE_TRAP) || !defined(FTL_LOGIC_ERROR_MODE_THROW) || !defined(FTL_LOGIC_ERROR_MODE_IGNORE)
#  define FTL_LOGIC_ERROR_MODE_TRAP
# endif
# if !defined(FTL_LENGTH_ERROR_MODE_TRAP) || !defined(FTL_LENGTH_ERROR_MODE_THROW) || !defined(FTL_LENGTH_ERROR_MODE_IGNORE)
#  define FTL_LENGTH_ERROR_MODE_TRAP
# endif
#elif defined(FTL_ERROR_MODE_THROW)
# if !defined(FTL_BOUNDS_ERROR_MODE_TRAP) || !defined(FTL_BOUNDS_ERROR_MODE_THROW) || !defined(FTL_BOUNDS_ERROR_MODE_IGNORE)
#  define FTL_BOUNDS_ERROR_MODE_THROW
# endif
# if !defined(FTL_LOGIC_ERROR_MODE_TRAP) || !defined(FTL_LOGIC_ERROR_MODE_THROW) || !defined(FTL_LOGIC_ERROR_MODE_IGNORE)
#  define FTL_LOGIC_ERROR_MODE_THROW
# endif
# if !defined(FTL_LENGTH_ERROR_MODE_TRAP) || !defined(FTL_LENGTH_ERROR_MODE_THROW) || !defined(FTL_LENGTH_ERROR_MODE_IGNORE)
#  define FTL_LENGTH_ERROR_MODE_THROW
# endif
#elif defined(FTL_ERROR_MODE_IGNORE)
# if !defined(FTL_BOUNDS_ERROR_MODE_TRAP) || !defined(FTL_BOUNDS_ERROR_MODE_THROW) || !defined(FTL_BOUNDS_ERROR_MODE_IGNORE)
#  define FTL_BOUNDS_ERROR_MODE_IGNORE
# endif
#endif

#if defined(FTL_BOUNDS_ERROR_MODE_TRAP)
# define FTL_BOUNDS_ERROR(x) __builtin_trap();
# define FTL_NOTHROW_BOUNDS_ERRORS true
#elif defined(FTL_BOUNDS_ERROR_MODE_THROW)
# if !FTL_EXCEPTIONS_ENABLED
#  error FTL_BOUNDS_ERROR_MODE_THROW is set but exceptions are not available
# endif
# define FTL_BOUNDS_ERROR(x) throw(std::out_of_range(x))
# define FTL_NOTHROW_BOUNDS_ERRORS false
#elif defined(FTL_BOUNDS_ERROR_MODE_IGNORE)
# define FTL_BOUNDS_ERROR(x) (void*)(0)
# define FTL_NOTHROW_BOUNDS_ERRORS true
#endif

#if defined(FTL_LOGIC_ERROR_MODE_TRAP)
# define FTL_LOGIC_ERROR(x) __builtin_trap();
# define FTL_NOTHROW_LOGIC_ERRORS true
#elif defined(FTL_LOGIC_ERROR_MODE_THROW)
# if !FTL_EXCEPTIONS_ENABLED
#  error FTL_LOGIC_ERROR_MODE_THROW is set but exceptions are not available
# endif
# define FTL_LOGIC_ERROR(x) throw(std::logic_error(x))
# define FTL_NOTHROW_LOGIC_ERRORS false
#elif defined(FTL_BOUNDS_ERROR_MODE_IGNORE)
# define FTL_LOGIC_ERROR(x) (void*)(0)
# define FTL_NOTHROW_LOGIC_ERRORS true
#endif

#if defined(FTL_LENGTH_ERROR_MODE_TRAP)
# define FTL_LENGTH_ERROR(x) __builtin_trap();
# define FTL_NOTHROW_LENGTH_ERRORS true
#elif defined(FTL_LENGTH_ERROR_MODE_THROW)
# if !FTL_EXCEPTIONS_ENABLED
#  error FTL_LENGTH_ERROR_MODE_THROW is set but exceptions are not available
# endif
# define FTL_LENGTH_ERROR(x) throw(std::length_error(x))
# define FTL_NOTHROW_LENGTH_ERRORS false
#elif defined(FTL_LENGTH_ERROR_MODE_IGNORE)
# define FTL_LENGTH_ERROR(x) (void*)(0)
# define FTL_NOTHROW_LENGTH_ERRORS true
#endif

#define FTL_MOVE(...) \
    static_cast<std::remove_reference_t<decltype(__VA_ARGS__)>&&>(__VA_ARGS__)

#define FTL_FORWARD(...) \
    static_cast<decltype(__VA_ARGS__)&&>(__VA_ARGS__)

#endif
/*
    Copyright 2024 Jari Ronkainen

    Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
    associated documentation files (the "Software"), to deal in the Software without restriction, including
    without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
    of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
    conditions:

    The above copyright notice and this permission notice shall be included in all copies or substantial portions
    of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
    INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
    PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
    LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
    OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
    DEALINGS IN THE SOFTWARE.
*/
