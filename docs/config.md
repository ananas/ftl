Configuring the library
=======================
FTL uses configuration macroes to configure some aspects of the library.  You can
define these with `#define FTL_*` before including any `ftl` headers.  Or better,
use your build system or compiler to add the definition.


Namespacing
-----------

### Summary

| Macro name                | Default value | Description                                           |
| ---                       | ---           | ---                                                   |
| `FTL_NAMESPACE`           | `ftl`         | namespace to use for everything ftl                   |

### `FTL_NAMESPACE`

Every `ftl` header uses `FTL_NAMESPACE` for its namespace.  By default this is just
`ftl` but it can be changed e.g. if ftl is used as a part of bigger project or set
of libraries and greater integration is warranted.


Error modes
-----------
`ftl` can be configured to handle certain errors with either trapping, throwing an exception or
ignoring the error altogether.  By default the errors cause the program to trap.  If the library
user wants throwing behaviour, that is doable via a compile-time switch.  These are provided so
the user can either increase compatibility with the standard library (which throws) or want to
have a way to recover from some errors.

`BOUNDS_ERROR` refers to out-of-bounds access, e.g. index out of range.  This is undefined behaviour,
but most standard library implementations **IGNORE** this error type (as of December 2024).

`LOGIC_ERROR` refers to faulty program logic.  e.g. giving invalid iterators to a range.  This is
similar to `BOUNDS_ERROR`.

`LENGTH_ERROR` is triggered when e.g. a vector is asked to grow larger than its `max_size()`.  In
these cases, the standard library **THROWS** a `std::length_error`.

`TRAP` mode instantly terminates the program abnormally.

`THROW` mode throws an exception

`IGNORE` eliminates the check branch from the code.  *You really, really shouldn't be using this
unless you think ~0.3% performance loss is too much compared to trapping.*  I'm not sure this
should even exist, and using it is almost always a bad idea.


### Summary

| Macro name                        | Description                                               |
| ---                               | ---                                                       |
| `FTL_DISABLE_EXCEPTIONS`          | disables use of exceptions from ftl headers               |
| ---                               | ---                                                       |
| `FTL_ERROR_MODE_TRAP`             | defining this will default all error modes to `TRAP`      |
| `FTL_ERROR_MODE_THROW`            | defining this will default all error modes to `THROW`     |
| `FTL_ERROR_MODE_IGNORE`           | defining this will default all error modes to `IGNORE`    |
| ---                               | ---                                                       |
| `FTL_BOUNDS_ERROR_MODE_TRAP`      | if defined, bounds errors trap                            |
| `FTL_BOUNDS_ERROR_MODE_THROW`     | if defined, bounds errors throw                           |
| `FTL_BOUNDS_ERROR_MODE_IGNORE`    | if defined, bounds errors are ignored                     |
| ---                               | ---                                                       |
| `FTL_LOGIC_ERROR_MODE_TRAP`       | if defined, logic errors trap                             |
| `FTL_LOGIC_ERROR_MODE_THROW`      | if defined, logic errors throw                            |
| `FTL_LOGIC_ERROR_MODE_IGNORE`     | if defined, logic errors are ignored                      |
| ---                               | ---                                                       |
| `FTL_LENGTH_ERROR_MODE_TRAP`      | if defined, length errors trap                            |
| `FTL_LENGTH_ERROR_MODE_THROW`     | if defined, length errors throw                           |
| `FTL_LENGTH_ERROR_MODE_IGNORE`    | if defined, length errors are ignored                     |

