# `ftl::array`

``` cpp
template <typename T, std::size_t... Dimensions>
class array;
```

`ftl:array` encapsulates fixed-size arrays.  The arrays may be multidimensional,
otherwise it is similar to `std::array`.  The order of elements in multidimensional
arrays follows C conventions.  It is possible to access the elements either with C-style
syntax of `array[x][y][z]`, a bit more conventional `array.at(x, y, z)` or C++23 `array[x, y, z]`

Note that `ftl::array` is not a drop-in replacement for `std::array`


## Requirements
Needs ftl `utility.hpp` and `hash.hpp` to be available


## Members
| Types             |                                                                   |
| -------           | -----------                                                       |
| `value_type`      | `T`                                                               |
| `reference`       | `T&`                                                              |
| `const_reference` | `const T&`                                                        |
| `size_type`       | `std::size_t`                                                     |
| `difference_type` | `std::ptrdiff_t`                                                  |
| `pointer`         | `T*`                                                              |
| `iterator`        | `T*`                                                              |
| `const_iterator`  | `const T*`                                                        |

| Static values     |                                                                   |
| -------           | -----------                                                       |
| `dimension`       | `sizeof...(Dimensions)`                                           |
| `size`            | `std::integral_constant` that is the size of the array            |
| `max_size`        | `std::integral_constant` that is the size of the array            |
| `capacity`        | `std::integral_constant` that is the size of the array            |

| Functions                     |                                                                                   |
| -------                       | -----------                                                                       |
| iterators                     |                                                                                   |
| `begin()`                     | return iterator to the beginning of buffer                                        |
| `end()`                       | return iterator to the end of buffer                                              |
| element access                |                                                                                   |
| `front()`                     | get reference to the first element without modifying the buffer                   |
| `back()`                      | get reference to the last element without modifying the buffer                    |
| `data()`                      | get pointer to the start of the array                                             |
| `operator[](size_type)`       | get reference to the nth element of the array, can be chained                     |
| `operator[](size_type, ...)`  | get reference to the requested element of the array (C++23 onward only)           |
| `at(size_type, ...)`          | get reference to the requested element of the array                               |
| modifiers                     |                                                                                   |
| `fill(const_reference)`       | fills the array with the element given                                            |
| `swap(ring_buffer&)`          | swaps ring buffer with another                                                    |
| queries                       |                                                                                   |
| `byte_size()`                 | returns number of bytes used by the array                                         |
| `is_empty()`                  | `true` if the container is of size 0, `false` otherwise                           |
| `extents()`                   | returns an array containing size of the dimensions                                |
| `extents(size_type)`          | returns size of the extent (or 0 if extent doesn't exist)                         |


## Exceptions
None

## Freestanding behaviour differences
None


## Example use
```cpp
#include <ftl/array.hpp>
#include <iostream>

int main()
{
    ftl::array<int, 3> a1{1, 2, 3};
    ftl::array<float, 3, 3> poor_mans_matrix{
        1, 2, 3
        4, 5, 6
        7, 8, 9
    };

    for (auto e : a1)
        std::cout << e << "\n";
}
```

