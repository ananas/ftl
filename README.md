FTL
===
Used to be: Freestanding Template Library, but since not everything is exactly
freestanding-compatible, I'm just going to call it FTL for now. 

Provides some classes / containers for common use that the standard library
does not.  *Tries* to make some of them freestanding-compatible
(such as ftl::array<>).  With a freestanding-compatible allocator most of
the allocator-aware containers can be used in freestanding, but there is
some work to be done before that is the case.

Types provided are not strictly drop-in-replacements to `std`, but quite close.
They do not make guarantees the standard does with iterator invalidation, etc.

Provides CMakeLists.txt and meson.build for easy integration.

All `ftl` headers use `FTL_NAMESPACE` definition as the namespace functionality
is defined in.  This defaults to `ftl`.  If more integration with user codebase
is wanted, it is possible to either `#define FTL_NAMESPACE <whatever you want>`
before including `ftl` headers or just pass `-DFTL_NAMESPACE <whatever you want>`
to the compiler via e.g. CMakeLists or meson.

Licence
-------
[MIT Licence](LICENCE.md)

The licence information is also contained at the *end* of the files,
so generally, as they are header-only, you are good as long as you don't
explicitly modify or remove the licence from those files no matter your use.

