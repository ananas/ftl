#include "../doctest.h"
#include "../test_common.hpp"

#include <type_traits>
#include <string>
#include <vector>
#include <array>
#include <ftl/sbovec.hpp>

// NOLINTBEGIN

template <typename T, typename... Args>
static T make_runtime(Args&&... args) {
    return T(std::forward<Args&&>(args)...);
}

template <typename T>
constexpr T get_test_value(std::size_t idx = 0) {
    if constexpr (std::is_arithmetic_v<T>) {
        return static_cast<T>(idx);
    }
    else if constexpr (std::same_as<std::string, T>) {
        if (idx == 0) return "zip";
        else if (idx == 1) return "uno";
        else if (idx == 2) return "dos";
        else if (idx == 3) return "san";
        else if (idx == 4) return "shi";
        else if (idx == 5) return "viisi";
        else if (idx == 6) return ":evergreen:";
        else if (idx == 7) return "seven";
        else if (idx == 8) return "eight";
        else if (idx == 9) return "nio";
        else return {};
    }
}

template <typename T>
ftl::sbovec<T> make_test_vec(std::size_t elems) {
    if constexpr (std::same_as<T, std::string>) {
        if (elems == 3) {
            return ftl::sbovec<std::string>{"one", "dos", "kolme"};
        }
        if (elems == 5) {
            return ftl::sbovec<std::string>{"one", "dos", "kolme", "four", "fünf"};
        }
    } else {
        if (elems == 3) {
            return ftl::sbovec<T>{1, 2, 3};
        }
        if (elems == 5) {
            return ftl::sbovec<T>{1, 2, 3, 4, 5};
        }
    }
    throw std::logic_error("invalid branch");
}

TEST_SUITE("ftl::sbovec") {

    /*
     *  initialisation
     */
    TEST_CASE("default initialisation") {
        SUBCASE("default-construction") {
            REQUIRE(std::is_default_constructible_v<ftl::sbovec<int>>);
        }
        SUBCASE("default-constructed container is empty") {
            auto trivial_vec = make_runtime<ftl::sbovec<ftl_test::trivial_type>>();

            CHECK(trivial_vec.size() == 0);
            CHECK(trivial_vec.is_empty() == true);
        }

        SUBCASE("default-constructed capacity matches requested") {
            auto trivial_vec = make_runtime<ftl::sbovec<ftl_test::trivial_type>>();

            // default cap
            CHECK(trivial_vec.capacity() == 2 * sizeof(ftl_test::trivial_type*) / sizeof(ftl_test::trivial_type));
            CHECK(not trivial_vec.is_full());
        }

        SUBCASE("default-constructing larger type than SBO limit") {
            auto big_vec = make_runtime<ftl::sbovec<std::array<uint8_t, 64>>>();
            CHECK(big_vec.capacity() == 0);
        }
    }

    TEST_CASE("list initialisation") {
        ftl::sbovec<int> sbo_vec{1, 2, 3};
        REQUIRE(sbo_vec.size() == 3);
        CHECK(*sbo_vec.data() + 0 == 1);
        CHECK(*sbo_vec.data() + 1 == 2);
        CHECK(*sbo_vec.data() + 2 == 3);

        ftl::sbovec<int> vec{1, 2, 3, 4, 5};
        REQUIRE(vec.size() == 5);
        CHECK(*vec.data() + 0 == 1);
        CHECK(*vec.data() + 1 == 2);
        CHECK(*vec.data() + 2 == 3);
        CHECK(*vec.data() + 3 == 4);
        CHECK(*vec.data() + 4 == 5);
    }

    TEST_CASE_TEMPLATE("copy construction", T, int, std::string) {
        auto sbo_vec = make_test_vec<T>(3);
        ftl::sbovec<T> copy(sbo_vec);

        REQUIRE(copy.size() == sbo_vec.size());
        for (std::size_t idx = 0; idx < sbo_vec.size(); ++idx)
            CHECK(*(copy.data() + idx) == *(sbo_vec.data() + idx));
    }

    TEST_CASE("move construction") {
        SUBCASE("sbo") {
            ftl::sbovec<int> moved_from{1, 2, 3};
            REQUIRE(moved_from.is_small());
            ftl::sbovec<int> moved_into(std::move(moved_from));

            REQUIRE(moved_into.size() == 3);
            for (std::size_t idx = 0; idx < moved_from.size(); ++idx)
                CHECK(*(moved_into.data() + idx) == idx + 1);
        }

        SUBCASE("no sbo") {
            ftl::sbovec<int> moved_from{1, 2, 3, 4, 5, 6};
            REQUIRE(not moved_from.is_small());
            ftl::sbovec<int> moved_into(std::move(moved_from));

            REQUIRE(moved_from.size() == 0);
            REQUIRE(moved_into.size() == 6);
            for (std::size_t idx = 0; idx < moved_from.size(); ++idx)
                CHECK(*(moved_into.data() + idx) == idx + 1);
        }
    }

    TEST_CASE("copy/move construct (longer string)") {
        ftl::sbovec<std::string> lipsum { "Lorem ipsum dolor sit amet,",
                                          "consectetur adipiscing elit,",
                                          "sed do eiusmod tempor incididunt"
                                          "ut labore et dolore magna aliqua."
                                          "Ut enim ad minim veniam, quis nostrud"
                                          "exercitation ullamco laboris nisi ut",
                                          "aliquip ex ea commodo consequat. Duis "
                                          "aute irure dolor in reprehenderit in voluptate "
                                          "velit esse cillum dolore eu fugiat nulla pariatur. ",
                                          "Excepteur sint occaecat cupidatat non proident, sunt "
                                          "in culpa qui officia deserunt mollit anim id est laborum." };
        ftl::sbovec<std::string> lipsum_copy(lipsum);

        REQUIRE(lipsum.size() == lipsum_copy.size());
        for (std::size_t idx = 0; idx < lipsum.size(); ++idx)
            CHECK(*(lipsum_copy.data() + idx) == *(lipsum.data() + idx));

        ftl::sbovec<std::string> moved_lipsum(std::move(lipsum_copy));
        REQUIRE(lipsum_copy.size() == 0);
        REQUIRE(lipsum.size() == moved_lipsum.size());
        for (std::size_t idx = 0; idx < lipsum.size(); ++idx)
            CHECK(*(moved_lipsum.data() + idx) == *(lipsum.data() + idx));
    }

    TEST_CASE_TEMPLATE("Construct with set size", T, int, std::string) {
        SUBCASE("Default constructed elements over SBO limit") {
            std::size_t count = ftl::sbovec<T>::sbo_limit + 10;

            ftl::sbovec<T> test_vec(count);
            REQUIRE(test_vec.size() == count);

            for(const auto& elem : test_vec)
                CHECK(elem == T{});
        }

        SUBCASE("Copy constructed elements over SBO limit") {
            std::size_t count = ftl::sbovec<T>::sbo_limit + 10;
            const T elem_copy = get_test_value<T>();

            ftl::sbovec<T> test_vec(count, elem_copy);
            REQUIRE(test_vec.size() == count);

            for(std::size_t i = 0; i < test_vec.size(); ++i)
                CHECK(*(test_vec.data() + i) == get_test_value<T>());
        }

        if constexpr (ftl::sbovec<T>::sbo_limit > 1) {
            SUBCASE("Default constructed elements below sbo limit") {
                std::size_t count = ftl::sbovec<T>::sbo_limit - 1;
                ftl::sbovec<T> test_vec(count);
                REQUIRE(test_vec.size() == count);
                for(const auto& elem : test_vec)
                    CHECK(elem == T{});
            }

            SUBCASE("Copy constructed elements below sbo limit") {
                std::size_t count = ftl::sbovec<T>::sbo_limit - 1;
                const T elem_copy = get_test_value<T>();
                ftl::sbovec<T> test_vec(count, elem_copy);
                REQUIRE(test_vec.size() == count);
                for(const auto& elem : test_vec)
                    CHECK(elem == elem_copy);
            }
        }
    }

    /*
     *  Assignment
     */
    TEST_CASE_TEMPLATE("Copy assignment via operator=", T, uint8_t, int, std::string) {
        SUBCASE("small to small") {
            std::size_t count = ftl::sbovec<T>::sbo_limit;
            ftl::sbovec<T> src;
            for (std::size_t i = 0; i < count; ++i)
                src.push_back(get_test_value<T>(i));

            ftl::sbovec<T> dst;
            REQUIRE(src.is_small());
            REQUIRE(dst.is_small());
            REQUIRE(src.size() == count);

            dst = src;

            REQUIRE(src.is_small());
            REQUIRE(dst.is_small());
            REQUIRE(src.size() == count);
            REQUIRE(dst.size() == src.size());
            for (std::size_t idx = 0; idx < dst.size(); ++idx) {
                CHECK(*(src.data() + idx) == get_test_value<T>(idx));
                CHECK(*(src.data() + idx) == *(dst.data() + idx));
            }
        }

        SUBCASE("large to large") {
            std::size_t count = ftl::sbovec<T>::sbo_limit + 2;
            ftl::sbovec<T> src;
            for (std::size_t i = 0; i < count; ++i)
                src.push_back(get_test_value<T>(i));

            ftl::sbovec<T> dst;
            for (std::size_t i = 0; i < count; ++i)
                dst.push_back(get_test_value<T>(i + 1));

            dst.clear();

            REQUIRE(not src.is_small());
            REQUIRE(not dst.is_small());
            REQUIRE(src.size() == count);
            REQUIRE(dst.size() == 0);

            dst = src;

            REQUIRE(not src.is_small());
            REQUIRE(not dst.is_small());
            REQUIRE(src.size() == count);
            REQUIRE(dst.size() == src.size());
            for (std::size_t idx = 0; idx < dst.size(); ++idx) {
                CHECK(*(src.data() + idx) == get_test_value<T>(idx));
                CHECK(*(src.data() + idx) == *(dst.data() + idx));
            }
        }

        SUBCASE("large to small") {
            std::size_t count = ftl::sbovec<T>::sbo_limit + 2;
            ftl::sbovec<T> src;
            for (std::size_t i = 0; i < count; ++i)
                src.push_back(get_test_value<T>(i));

            ftl::sbovec<T> dst;
            REQUIRE(not src.is_small());
            REQUIRE(dst.is_small());

            dst = src;

            REQUIRE(not src.is_small());
            REQUIRE(not dst.is_small());
            REQUIRE(src.size() == count);
            REQUIRE(dst.size() == src.size());
            for (std::size_t idx = 0; idx < dst.size(); ++idx) {
                CHECK(*(src.data() + idx) == get_test_value<T>(idx));
                CHECK(*(src.data() + idx) == *(dst.data() + idx));
            }
        }

        SUBCASE("small to large") {
            std::size_t count = ftl::sbovec<T>::sbo_limit;
            ftl::sbovec<T> src;
            ftl::sbovec<T> dst;
            for (std::size_t i = 0; i < count; ++i) {
                src.push_back(get_test_value<T>(i));
                dst.push_back(get_test_value<T>());
            }
            dst.push_back(get_test_value<T>());

            REQUIRE(src.is_small());
            REQUIRE(not dst.is_small());

            dst = src;

            REQUIRE(src.is_small());
            REQUIRE(src.size() == count);
            REQUIRE(dst.size() == src.size());
            for (std::size_t idx = 0; idx < dst.size(); ++idx) {
                CHECK(*(dst.data() + idx) == get_test_value<T>(idx));
                CHECK(*(src.data() + idx) == *(dst.data() + idx));
            }
        }
    }

    TEST_CASE_TEMPLATE("Move assignment via operator=", T, uint8_t, int, std::string) {
        SUBCASE("small to small") {
            std::size_t count = ftl::sbovec<T>::sbo_limit;
            ftl::sbovec<T> src;
            for (std::size_t i = 0; i < count; ++i)
                src.push_back(get_test_value<T>(i));

            ftl::sbovec<T> dst;
            REQUIRE(src.is_small());
            REQUIRE(dst.is_small());
            REQUIRE(src.size() == count);

            dst = std::move(src);

            REQUIRE(dst.is_small());
            REQUIRE(dst.size() == count);
            for (std::size_t idx = 0; idx < dst.size(); ++idx) {
                CHECK(*(dst.data() + idx) == get_test_value<T>(idx));
            }
        }

        SUBCASE("large to large") {
            std::size_t count = ftl::sbovec<T>::sbo_limit + 2;
            ftl::sbovec<T> src;
            for (std::size_t i = 0; i < count; ++i)
                src.push_back(get_test_value<T>(i));

            ftl::sbovec<T> dst;
            for (std::size_t i = 0; i < count; ++i)
                dst.push_back(get_test_value<T>(i + 1));

            dst.clear();

            REQUIRE(not src.is_small());
            REQUIRE(not dst.is_small());
            REQUIRE(src.size() == count);
            REQUIRE(dst.size() == 0);

            dst = std::move(src);

            REQUIRE(not dst.is_small());
            REQUIRE(dst.size() == count);
            for (std::size_t idx = 0; idx < dst.size(); ++idx) {
                CHECK(*(dst.data() + idx) == get_test_value<T>(idx));
            }
        }

        SUBCASE("large to small") {
            std::size_t count = ftl::sbovec<T>::sbo_limit + 2;
            ftl::sbovec<T> src;
            for (std::size_t i = 0; i < count; ++i)
                src.push_back(get_test_value<T>(i));

            ftl::sbovec<T> dst;
            REQUIRE(not src.is_small());
            REQUIRE(dst.is_small());

            dst = std::move(src);

            REQUIRE(not dst.is_small());
            REQUIRE(dst.size() == count);
            for (std::size_t idx = 0; idx < dst.size(); ++idx) {
                CHECK(*(dst.data() + idx) == get_test_value<T>(idx));
            }
        }

        SUBCASE("small to large") {
            std::size_t count = ftl::sbovec<T>::sbo_limit;
            ftl::sbovec<T> src;
            ftl::sbovec<T> dst;
            for (std::size_t i = 0; i < count; ++i) {
                src.push_back(get_test_value<T>(i));
                dst.push_back(get_test_value<T>());
            }
            dst.push_back(get_test_value<T>());

            REQUIRE(src.is_small());
            REQUIRE(not dst.is_small());

            dst = std::move(src);

            REQUIRE(dst.size() == count);
            for (std::size_t idx = 0; idx < dst.size(); ++idx) {
                CHECK(*(dst.data() + idx) == get_test_value<T>(idx));
            }
        }
    }


    /*
     *  access
     */
    TEST_CASE("front() / back()") {
        SUBCASE("size() < sbo_limit") {
            ftl::sbovec<int> vec{1, 2, 3};
            REQUIRE(vec.is_small());
            CHECK(vec.front() == 1);
            CHECK(vec.back() == 3);

            const ftl::sbovec<int> const_vec{1, 2, 3};
            REQUIRE(const_vec.is_small());
            CHECK(const_vec.front() == 1);
            CHECK(const_vec.back() == 3);
        }

        SUBCASE("size() >= sbo_limit") {
        }
    }

    TEST_CASE_TEMPLATE("write/read round trip via operator[]", T, int, std::string) {
        ftl::sbovec<T> test_vec(10);
        REQUIRE(test_vec.size() == 10);

        for(std::size_t i = 0; i < test_vec.size(); ++i)
            test_vec[i] = get_test_value<T>(i);

        for (std::size_t i = 0; i < test_vec.size(); ++i) {
            const auto value = const_cast<const ftl::sbovec<T>&>(test_vec)[i];
            CHECK(value == get_test_value<T>(i));
            CHECK(test_vec[i] == *(test_vec.data() + i));
            CHECK(test_vec[i] == test_vec.at(i));
        }
    }

    TEST_CASE_TEMPLATE("write/read round trip via at()", T, int, std::string) {
        ftl::sbovec<T> test_vec(10);
        REQUIRE(test_vec.size() == 10);

        for(std::size_t i = 0; i < test_vec.size(); ++i)
            test_vec.at(i) = get_test_value<T>(i);

        for (std::size_t i = 0; i < test_vec.size(); ++i) {
            const auto value = const_cast<const ftl::sbovec<T>&>(test_vec).at(i);
            CHECK(value == get_test_value<T>(i));
            CHECK(test_vec.at(i) == *(test_vec.data() + i));
            CHECK(test_vec.at(i) == test_vec[i]);
        }
    }

    /*
     *  comparison
     */
    TEST_CASE_TEMPLATE("equality comparison", T, int, std::string) {
        ftl::sbovec<T> vec_a = {
            get_test_value<T>(0),
            get_test_value<T>(1),
            get_test_value<T>(2),
        };

        ftl::sbovec<T> vec_b = {
            get_test_value<T>(0),
            get_test_value<T>(1),
            get_test_value<T>(2),
        };

        CHECK(vec_a == vec_b);
    }

    /*
     *  reserve / resize
     */
    TEST_CASE("reserve") {
        auto vec = make_runtime<ftl::sbovec<int>>();

        SUBCASE("capacity preserves elements") {
            ftl::sbovec<int> vec{1, 2, 3, 4, 5};
            REQUIRE(vec.size() == 5);
            REQUIRE(*vec.data() + 0 == 1);
            REQUIRE(*vec.data() + 1 == 2);
            REQUIRE(*vec.data() + 2 == 3);
            REQUIRE(*vec.data() + 3 == 4);
            REQUIRE(*vec.data() + 4 == 5);
            std::size_t old_capacity = vec.capacity();
            vec.reserve(old_capacity + 1);
            REQUIRE(vec.size() == 5);
            REQUIRE(vec.capacity() >= old_capacity + 1);
            CHECK(*vec.data() + 0 == 1);
            CHECK(*vec.data() + 1 == 2);
            CHECK(*vec.data() + 2 == 3);
            CHECK(*vec.data() + 3 == 4);
            CHECK(*vec.data() + 4 == 5);
        }

        CHECK(vec.capacity() > 0);
    }

    TEST_CASE("resize (int)") {
        ftl::sbovec<int> vec{0, 1, 2, 3, 4, 5};
        SUBCASE("to smaller size") {
            std::size_t orig_capacity = vec.capacity();
            vec.resize(3);
            REQUIRE(vec.size() == 3);

            for (std::size_t i = 0; i < 3; ++i)
                CHECK(*(vec.data() + i) == i);

            CHECK(vec.capacity() == orig_capacity);
        }

        SUBCASE("to same size") {
            vec.resize(6);
        }

        SUBCASE("to larger size") {
            vec.resize(10);

            for (std::size_t i = 0; i < 10; ++i) {
                if (i < 6)
                    CHECK(*(vec.data() + i) == i);
                else
                    CHECK(*(vec.data() + i) == 0);
            }
        }

        SUBCASE("to larger size (copy init)") {
            vec.resize(10, 42);
            for (std::size_t i = 0; i < 10; ++i) {
                if (i < 6)
                    CHECK(*(vec.data() + i) == i);
                else
                    CHECK(*(vec.data() + i) == 42);
            }
        }
    }

    TEST_CASE("resize (std::string)") {
        ftl::sbovec<std::string> vec{"one", "dos", "san", "neljä", "quint", ":evergreen:"};
        SUBCASE("to smaller size") {
            std::size_t orig_capacity = vec.capacity();
            vec.resize(3);
            REQUIRE(vec.size() == 3);

            CHECK(*(vec.data() + 0) == "one");
            CHECK(*(vec.data() + 1) == "dos");
            CHECK(*(vec.data() + 2) == "san");

            CHECK(vec.capacity() == orig_capacity);
        }

        SUBCASE("to same size") {
            vec.resize(6);
        }

        SUBCASE("to larger size") {
            vec.resize(10);

            CHECK(*(vec.data() + 0) == "one");
            CHECK(*(vec.data() + 1) == "dos");
            CHECK(*(vec.data() + 2) == "san");
            CHECK(*(vec.data() + 3) == "neljä");
            CHECK(*(vec.data() + 4) == "quint");
            CHECK(*(vec.data() + 5) == ":evergreen:");

            CHECK((vec.data() + 6)->empty());
            CHECK((vec.data() + 7)->empty());
            CHECK((vec.data() + 8)->empty());
            CHECK((vec.data() + 9)->empty());
        }

        SUBCASE("to larger size (copy init)") {
            vec.resize(10, "pineapples belong to pizza");

            CHECK(*(vec.data() + 0) == "one");
            CHECK(*(vec.data() + 1) == "dos");
            CHECK(*(vec.data() + 2) == "san");
            CHECK(*(vec.data() + 3) == "neljä");
            CHECK(*(vec.data() + 4) == "quint");
            CHECK(*(vec.data() + 5) == ":evergreen:");

            for (std::size_t i = 6; i < 10; ++i) {
                CHECK(*(vec.data() + i) == "pineapples belong to pizza");
            }
        }
    }

    /*
     *  push / emplace / insert
     */
    TEST_CASE_TEMPLATE("push_back", T, int, std::string) {
        SUBCASE("default construct a value and push (lvalue)") {
            ftl::sbovec<T> vec;
            T val{};
            vec.push_back(val);
            CHECK(vec.size() == 1);
            vec.push_back(val);
            CHECK(vec.size() == 2);
            vec.push_back(val);
            CHECK(vec.size() == 3);
        }

        SUBCASE("default construct a temporary and push (rvalue)") {
            ftl::sbovec<T> vec;
            vec.push_back(T{});
            CHECK(vec.size() == 1);
            vec.push_back(T{});
            CHECK(vec.size() == 2);
            vec.push_back(T{});
            CHECK(vec.size() == 3);
        }
    }

    TEST_CASE_TEMPLATE("emplace_back", T, int, std::string) {
        SUBCASE("default construct a value and push (lvalue)") {
            ftl::sbovec<T> vec;
            T val{};
            vec.emplace_back(val);
            CHECK(vec.size() == 1);
            vec.emplace_back(val);
            CHECK(vec.size() == 2);
            vec.emplace_back(val);
            CHECK(vec.size() == 3);
        }

        SUBCASE("default construct a value and push (rvalue)") {
            ftl::sbovec<T> vec;
            vec.emplace_back(T{});
            CHECK(vec.size() == 1);
            vec.emplace_back(T{});
            CHECK(vec.size() == 2);
            vec.emplace_back(T{});
            CHECK(vec.size() == 3);
        }
    }

    TEST_CASE("push past the sbo limit") {
        ftl::sbovec<uint8_t> vec;
        for (uint8_t i = 0; i < ftl::sbovec<uint8_t>::sbo_limit; ++i) {
            vec.push_back(i);
        }
        REQUIRE(vec.is_full());
        REQUIRE(vec.is_small());

        vec.push_back(static_cast<uint8_t>(ftl::sbovec<uint8_t>::sbo_limit));

        CHECK(vec.capacity() > ftl::sbovec<uint8_t>::sbo_limit);
        CHECK(vec.size() == ftl::sbovec<uint8_t>::sbo_limit + 1);
        for (uint8_t i = 0; i < vec.size(); ++i) {
            CHECK(*(vec.data() + i) == i);
        }
    }

    /*
     *  pop
     */
    TEST_CASE("pop_back()") {
        SUBCASE("within SBO limits") {
            ftl::sbovec<uint8_t> vec;
            for (uint8_t i = 0; i < ftl::sbovec<uint8_t>::sbo_limit; ++i) {
                vec.push_back(i);
            }
            REQUIRE(vec.is_full());
            REQUIRE(vec.is_small());

            auto popped_value = vec.pop_back();

            REQUIRE(not vec.is_full());
            CHECK(vec.size() == ftl::sbovec<uint8_t>::sbo_limit - 1);
            CHECK(popped_value == vec.size());
        }

        SUBCASE("out of SBO limit") {
            ftl::sbovec<uint8_t> vec;
            for (uint8_t i = 0; i < ftl::sbovec<uint8_t>::sbo_limit + 2; ++i) {
                vec.push_back(i);
            }
            REQUIRE(not vec.is_small());

            vec.pop_back();
            CHECK(vec.size() == ftl::sbovec<uint8_t>::sbo_limit + 1);
            vec.pop_back();
            CHECK(vec.size() == ftl::sbovec<uint8_t>::sbo_limit);
            vec.pop_back();
            CHECK(vec.size() == ftl::sbovec<uint8_t>::sbo_limit - 1);
        }
    }

    struct pair
    {
        int asdf;
        std::string bsdf;
    };

    /*
     *  insert
     */
    TEST_CASE("emplace()") {
        ftl::sbovec<pair> vec;
        vec.emplace(vec.end(), 42, "moi");
        REQUIRE(vec.size() == 1);
        CHECK(vec.data()->asdf == 42);
        CHECK(vec.data()->bsdf == "moi");
    }

    TEST_CASE_TEMPLATE("insert()", T, uint8_t, int, std::string) {
        if constexpr (ftl::sbovec<T>::sbo_limit > 1) {
            SUBCASE("into empty vector, single, without reaching SBO limit (copy)") {
                T value = get_test_value<T>();
                ftl::sbovec<T> vec;
                auto it = vec.insert(vec.end(), value);
                REQUIRE(vec.size() == 1);
                CHECK(*(vec.data()) == get_test_value<T>());
                CHECK(*it == get_test_value<T>());
            }

            SUBCASE("into empty vector, single, without reaching SBO limit (move)") {
                ftl::sbovec<T> vec;
                auto it = vec.insert(vec.end(), get_test_value<T>());
                REQUIRE(vec.size() == 1);
                CHECK(*(vec.data()) == get_test_value<T>());
                CHECK(*it == get_test_value<T>());
            }
        }
        if constexpr (ftl::sbovec<T>::sbo_limit > 3) {
            SUBCASE("into empty vector, multiple, without reaching SBO limit") {
                ftl::sbovec<T> vec;
                std::vector<T> src = {
                    get_test_value<T>(0),
                    get_test_value<T>(1),
                    get_test_value<T>(2),
                };
                auto it = vec.insert(vec.end(), src.begin(), src.end());
                REQUIRE(vec.is_small());
                REQUIRE(vec.size() == 3);
                CHECK(*(vec.data() + 0) == get_test_value<T>(0));
                CHECK(*(vec.data() + 1) == get_test_value<T>(1));
                CHECK(*(vec.data() + 2) == get_test_value<T>(2));
                CHECK(*it == get_test_value<T>(0));
            }

            SUBCASE("into empty vector, from initializer_list, without reaching SBO limit") {
                ftl::sbovec<T> vec;
                auto it = vec.insert(vec.end(), {
                    get_test_value<T>(0),
                    get_test_value<T>(1),
                    get_test_value<T>(2)
                });
                REQUIRE(vec.is_small());
                REQUIRE(vec.size() == 3);
                CHECK(*(vec.data() + 0) == get_test_value<T>(0));
                CHECK(*(vec.data() + 1) == get_test_value<T>(1));
                CHECK(*(vec.data() + 2) == get_test_value<T>(2));
                CHECK(*it == get_test_value<T>(0));
            }

            SUBCASE("into beginning of a vector, single, without reaching SBO limit (copy)") {
                ftl::sbovec<T> vec{ get_test_value<T>(0), get_test_value<T>(1) };
                T value = get_test_value<T>(2);
                vec.insert(vec.begin(), value);
                REQUIRE(vec.size() == 3);
                CHECK(*(vec.data() + 0) == get_test_value<T>(2));
                CHECK(*(vec.data() + 1) == get_test_value<T>(0));
                CHECK(*(vec.data() + 2) == get_test_value<T>(1));
            }

            SUBCASE("into beginning of a vector, single, without reaching SBO limit (move)") {
                ftl::sbovec<T> vec{ get_test_value<T>(0), get_test_value<T>(1) };
                vec.insert(vec.begin(), get_test_value<T>(2));
                REQUIRE(vec.size() == 3);
                CHECK(*(vec.data() + 0) == get_test_value<T>(2));
                CHECK(*(vec.data() + 1) == get_test_value<T>(0));
                CHECK(*(vec.data() + 2) == get_test_value<T>(1));
            }
        }
        if constexpr (ftl::sbovec<T>::sbo_limit > 5) {
            SUBCASE("into beginning of a vector, multiple, without reaching SBO limit") {
                ftl::sbovec<T> vec{ get_test_value<T>(0), get_test_value<T>(1) };
                std::vector<T> src = {
                    get_test_value<T>(2),
                    get_test_value<T>(3),
                    get_test_value<T>(4),
                };
                auto it = vec.insert(vec.begin(), src.begin(), src.end());
                REQUIRE(vec.is_small());
                REQUIRE(vec.size() == 5);
                CHECK(*(vec.data() + 0) == get_test_value<T>(2));
                CHECK(*(vec.data() + 1) == get_test_value<T>(3));
                CHECK(*(vec.data() + 2) == get_test_value<T>(4));
                CHECK(*(vec.data() + 3) == get_test_value<T>(0));
                CHECK(*(vec.data() + 4) == get_test_value<T>(1));
                CHECK(*it == get_test_value<T>(2));
            }

            SUBCASE("into the middle of a vector, multiple, without reaching SBO limit") {
                ftl::sbovec<T> vec{ get_test_value<T>(0), get_test_value<T>(1) };
                std::vector<T> src = {
                    get_test_value<T>(2),
                    get_test_value<T>(3),
                    get_test_value<T>(4),
                };
                auto it = vec.insert(vec.begin() + 1, src.begin(), src.end());
                REQUIRE(vec.is_small());
                REQUIRE(vec.size() == 5);
                CHECK(*(vec.data() + 0) == get_test_value<T>(0));
                CHECK(*(vec.data() + 1) == get_test_value<T>(2));
                CHECK(*(vec.data() + 2) == get_test_value<T>(3));
                CHECK(*(vec.data() + 3) == get_test_value<T>(4));
                CHECK(*(vec.data() + 4) == get_test_value<T>(1));
                CHECK(*it == get_test_value<T>(2));
            }
        };
        SUBCASE("into the end of a vector, single, crossing SBO limit (copy)") {
            ftl::sbovec<T> vec(ftl::sbovec<T>::sbo_limit);
            REQUIRE(vec.is_small());
            T value = get_test_value<T>();
            auto orig_size = vec.size();
            auto it = vec.insert(vec.end(), value);

            REQUIRE(vec.size() == orig_size + 1);
            REQUIRE(not vec.is_small());
            CHECK(it == vec.end() - 1);
            CHECK(*it == get_test_value<T>());
        }
        SUBCASE("into the end of a vector, single, crossing SBO limit (move)") {
            ftl::sbovec<T> vec(ftl::sbovec<T>::sbo_limit);
            REQUIRE(vec.is_small());
            auto orig_size = vec.size();
            auto it = vec.insert(vec.end(), get_test_value<T>());

            REQUIRE(vec.size() == orig_size + 1);
            REQUIRE(not vec.is_small());
            CHECK(it == vec.end() - 1);
            CHECK(*it == get_test_value<T>());
        }
        SUBCASE("into the end of a vector, multiple, crossing SBO limit") {
            ftl::sbovec<T> vec(ftl::sbovec<T>::sbo_limit);
            REQUIRE(vec.is_small());
            auto orig_size = vec.size();
            std::vector<T> src = {
                get_test_value<T>(2),
                get_test_value<T>(3),
                get_test_value<T>(4),
            };
            auto it = vec.insert(vec.end(), src.begin(), src.end());
            REQUIRE(not vec.is_small());
            REQUIRE(vec.size() == orig_size + 3);
            CHECK(it == vec.end() - 3);
            CHECK(*(it + 0) == get_test_value<T>(2));
            CHECK(*(it + 1) == get_test_value<T>(3));
            CHECK(*(it + 2) == get_test_value<T>(4));
        }
        SUBCASE("into the beginning of a vector, single, crossing SBO limit (copy)") {
            ftl::sbovec<T> vec(ftl::sbovec<T>::sbo_limit);
            REQUIRE(vec.is_small());
            T value = get_test_value<T>();
            auto orig_size = vec.size();
            auto it = vec.insert(vec.begin(), value);

            REQUIRE(vec.size() == orig_size + 1);
            REQUIRE(not vec.is_small());
            CHECK(it == vec.begin());
            CHECK(*it == get_test_value<T>());
        }
        SUBCASE("into the beginning of a vector, single, crossing SBO limit (move)") {
            ftl::sbovec<T> vec(ftl::sbovec<T>::sbo_limit);
            REQUIRE(vec.is_small());
            auto orig_size = vec.size();
            auto it = vec.insert(vec.begin(), get_test_value<T>());

            REQUIRE(vec.size() == orig_size + 1);
            REQUIRE(not vec.is_small());
            CHECK(it == vec.begin());
            CHECK(*it == get_test_value<T>());
        }
        SUBCASE("into the beginning of a vector, multiple, crossing SBO limit") {
            ftl::sbovec<T> vec(ftl::sbovec<T>::sbo_limit);
            REQUIRE(vec.is_small());
            auto orig_size = vec.size();
            std::vector<T> src = {
                get_test_value<T>(2),
                get_test_value<T>(3),
                get_test_value<T>(4),
            };
            auto it = vec.insert(vec.begin(), src.begin(), src.end());
            REQUIRE(not vec.is_small());
            REQUIRE(vec.size() == orig_size + 3);
            CHECK(it == vec.begin());
            CHECK(*(vec.begin() + 0) == get_test_value<T>(2));
            CHECK(*(vec.begin() + 1) == get_test_value<T>(3));
            CHECK(*(vec.begin() + 2) == get_test_value<T>(4));
        }
    }

    /*
     *  erase
     */
    TEST_CASE_TEMPLATE("erase()", T, int, std::string) {
        SUBCASE("erase only element") {
            { // via begin()
                ftl::sbovec<T> vec{ get_test_value<T>() };
                auto it = vec.erase(vec.begin());
                REQUIRE(vec.empty());
                REQUIRE(vec.size() == 0);
                CHECK(it == vec.end());
            }
        }
        SUBCASE("erase single element from the beginning of the vector") {
            ftl::sbovec<T> vec{
                get_test_value<T>(0),
                get_test_value<T>(1),
                get_test_value<T>(2)
            };
            auto it = vec.erase(vec.begin());
            REQUIRE(vec.size() == 2);
            CHECK(*(vec.data() + 0) == get_test_value<T>(1));
            CHECK(*(vec.data() + 1) == get_test_value<T>(2));
            CHECK(it == vec.begin());
            CHECK(*it == get_test_value<T>(1));
        }
        SUBCASE("erase single element from the middle of the vector") {
            ftl::sbovec<T> vec{
                get_test_value<T>(0),
                get_test_value<T>(1),
                get_test_value<T>(2)
            };
            auto it = vec.erase(vec.begin() + 1);
            REQUIRE(vec.size() == 2);
            CHECK(*(vec.data() + 0) == get_test_value<T>(0));
            CHECK(*(vec.data() + 1) == get_test_value<T>(2));
            CHECK(it == vec.begin() + 1);
            CHECK(*it == get_test_value<T>(2));
        }
        SUBCASE("erase single element from the end of the vector") {
            ftl::sbovec<T> vec{
                get_test_value<T>(0),
                get_test_value<T>(1),
                get_test_value<T>(2)
            };
            auto it = vec.erase(vec.begin() + 2);
            REQUIRE(vec.size() == 2);
            REQUIRE(it == vec.end());
            CHECK(*(vec.data() + 0) == get_test_value<T>(0));
            CHECK(*(vec.data() + 1) == get_test_value<T>(1));
        }
        SUBCASE("erase multiple elements from beginning of the vector") {
            ftl::sbovec<T> vec{
                get_test_value<T>(0),
                get_test_value<T>(1),
                get_test_value<T>(2)
            };
            auto it = vec.erase(vec.begin(), vec.begin() + 2);
            REQUIRE(vec.size() == 1);
            CHECK(*(vec.data()) == get_test_value<T>(2));
            CHECK(it == vec.begin());
            CHECK(*it == get_test_value<T>(2));
        }
        SUBCASE("erase multiple elements from middle of the vector") {
            ftl::sbovec<T> vec{
                get_test_value<T>(0),
                get_test_value<T>(1),
                get_test_value<T>(2),
                get_test_value<T>(3)
            };
            auto it = vec.erase(vec.begin() + 1, vec.begin() + 3);
            REQUIRE(vec.size() == 2);
            CHECK(*(vec.data() + 0) == get_test_value<T>(0));
            CHECK(*(vec.data() + 1) == get_test_value<T>(3));
            CHECK(it == vec.begin() + 1);
            CHECK(*it == get_test_value<T>(3));
        }
        SUBCASE("erase multiple elements from the end of the vector") {
            ftl::sbovec<T> vec{
                get_test_value<T>(0),
                get_test_value<T>(1),
                get_test_value<T>(3),
            };
            auto it = vec.erase(vec.begin() + 1, vec.begin() + 3);
            REQUIRE(vec.size() == 1);
            REQUIRE(it == vec.end());
            CHECK(*(vec.data()) == get_test_value<T>(0));
        }
    }

    /*
     *  swap
     */
    TEST_CASE_TEMPLATE("swap()", T, uint8_t, int, std::string) {
        if constexpr (ftl::sbovec<T>::sbo_limit >= 3) {
            SUBCASE("member function swap (both vectors using sbo)") {
                ftl::sbovec<T> vec_a{
                    get_test_value<T>(0),
                    get_test_value<T>(1),
                    get_test_value<T>(2),
                };

                ftl::sbovec<T> vec_b{
                    get_test_value<T>(3),
                    get_test_value<T>(4),
                    get_test_value<T>(5),
                };
                REQUIRE(vec_a.is_small());
                REQUIRE(vec_b.is_small());

                vec_a.swap(vec_b);
                CHECK(*(vec_a.data() + 0) == get_test_value<T>(3));
                CHECK(*(vec_a.data() + 1) == get_test_value<T>(4));
                CHECK(*(vec_a.data() + 2) == get_test_value<T>(5));

                CHECK(*(vec_b.data() + 0) == get_test_value<T>(0));
                CHECK(*(vec_b.data() + 1) == get_test_value<T>(1));
                CHECK(*(vec_b.data() + 2) == get_test_value<T>(2));
            }
            SUBCASE("free function swap") {
                ftl::sbovec<T> vec_a{
                    get_test_value<T>(0),
                    get_test_value<T>(1),
                    get_test_value<T>(2),
                };

                ftl::sbovec<T> vec_b{
                    get_test_value<T>(3),
                    get_test_value<T>(4),
                    get_test_value<T>(5),
                };
                REQUIRE(vec_a.is_small());
                REQUIRE(vec_b.is_small());

                swap(vec_a, vec_b);

                CHECK(*(vec_a.data() + 0) == get_test_value<T>(3));
                CHECK(*(vec_a.data() + 1) == get_test_value<T>(4));
                CHECK(*(vec_a.data() + 2) == get_test_value<T>(5));

                CHECK(*(vec_b.data() + 0) == get_test_value<T>(0));
                CHECK(*(vec_b.data() + 1) == get_test_value<T>(1));
                CHECK(*(vec_b.data() + 2) == get_test_value<T>(2));
            }
        }
    }
}

// NOLINTEND
